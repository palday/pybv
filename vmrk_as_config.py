
# coding: utf-8

# In[1]:


import configparser
from io import StringIO
import pandas as pd
import re


# In[2]:


with open('testdata/testdata0004.vmrk') as fin: cfgdat = fin.read()


# In[3]:


print(cfgdat)


# In[4]:


header, cfgdat = cfgdat.split('\n',1)


# In[5]:


header


# In[6]:


print(cfgdat)


# In[7]:


config = configparser.ConfigParser()
config.optionxform = lambda option: option
config.read_string(cfgdat)


# In[8]:


dict(config['Common Infos'])


# In[9]:


dict(config['Marker Infos'])


# In[10]:


def parse_marker(mkstr):
    '''Parse marker info from VMRK file.'''
    fields = mkstr.split(',')
    
    if len(fields) == 5:
        fields.append(None)
    
    type, desc, pos, size, ch, timestamp  = fields
    
    return dict(type=type,
                description=desc,
                position=pos,
                duration=size,
                channel=ch,
                timestamp=timestamp)
parse_marker(config["Marker Infos"]["Mk1"]), parse_marker(config["Marker Infos"]["Mk2"])


# In[11]:


vmrk = dict()
for section in config:
    vmrk[section] = dict(config[section])
    
for mk in vmrk['Marker Infos']:
    vmrk['Marker Infos'][mk] = parse_marker(vmrk['Marker Infos'][mk])


vmrk


# TODO
# - Version 1.0 vs. Version 2.0
# - S vs R vs No Prefixes 
# - Find Marker User Infos example
# - Convert \1 to Commas
# - Deal with CodePage
